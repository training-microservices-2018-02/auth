# Authorization Server #

## Flow Grant Type Authorization Code ##

1. Buka URL provider Authorization

        http://localhost:8080/oauth/authorize?client_id=client001&response_type=code&redirect_uri=http://localhost:10000/handle-oauth-callback

2. Login bila perlu

3. Approve di consent page

4. Mendapatkan authorization code di URL redirect : `http://localhost:10000/?code=JT5N6T`

5. Menukarkan authorization code dengan access token & refresh token

    * URL : http://localhost:8080/oauth/token
    * Request Method : POST
    * Header : Basic Auth (client id & client secret), Accept : application/json
    * Parameter : grant_type : authorization_code, code : JT5N6T, redirect_uri : http://localhost:10000/handle-oauth-callback

    ```json
    {
        "access_token": "250ea88c-27e4-4a52-a9ab-87ddea8b5a93",
        "token_type": "bearer",
        "refresh_token": "add54c7d-108b-4380-afcd-78959ce1801b",
        "expires_in": 43199,
        "scope": "all"
    }
    ```

6. Pakai access token di resource server

## Refresh Token ##

Digunakan untuk memperbarui access_token yang expire.

1. Request dengan perintah seperti ini

        curl -X POST -vu client001:abcd http://localhost:8080/oauth/token -d "client_id=client001&grant_type=refresh_token&refresh_token=add54c7d-108b-4380-afcd-78959ce1801b"
    
2. Hasilnya

    ```json
    {
        "access_token": "9b0411b1-e3ca-45ce-8c45-9c86cda9bef7",
        "token_type": "bearer",
        "refresh_token": "add54c7d-108b-4380-afcd-78959ce1801b",
        "expires_in": 43199,
        "scope": "all"
    }
    ```

## JSON Web Token (JWT) ##

1. Generate keystore untuk menyimpan public key

        keytool -genkey -keystore authstore.p12 -alias aplikasiauth -storetype PKCS12 -keyalg RSA 

2. Jawab pertanyaan, isi passwordnya `abcd123`

        Enter keystore password: 123456
        Re-enter new password: 123456
        What is your first and last name?
        [Unknown]:  Auth Server
        What is the name of your organizational unit?
        [Unknown]:  Belajar OAuth
        What is the name of your organization?
        [Unknown]:  ArtiVisi
        What is the name of your City or Locality?
        [Unknown]:  Jakarta Timur
        What is the name of your State or Province?
        [Unknown]:  Jakarta
        What is the two-letter country code for this unit?
        [Unknown]:  ID
        Is CN=Auth Server, OU=Belajar OAuth, O=ArtiVisi, L=Jakarta Timur, ST=Jakarta, C=ID correct?
        [no]:  yes

3. Public key bisa diakses di `http://localhost:8080/oauth/token_key`

    ```json
    {
        "alg":"SHA256withRSA",
        "value":"-----BEGIN PUBLIC KEY----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlOJzUtkJF0L8dLm1Ia8HO65CJq+YGRXh5ygGgmY2UMeEbRyTlyuGvxPwC5VieTF2A12CZuZHIZi9YNGA0p5Gw7PExLkYTrjVymr5vhzNmX5o/98Gd7bBSgidNs4bPFoi1Dh7AdlTTgSobZRGvB9X5wyxYPNfE7BqQAlWwm/8fHDB61HMbACXbphSAspSYgfAcFrAj4wcO2HQH/Uil7ObC3O5co+Rcpc9qHuQ0KxOgh1WyZhGW7t3Oo+zYx4SAydxrlfZ/eue0nnubDoxkQFQjspF9qGhqI7k+9tGZrqdKl3ckaBQ9YtN5MkhF4S6dy5ywS3xMCBA3r72GK0/U/seMQIDAQAB\n-----END PUBLIC KEY-----"
    }
    ```

4. Setup di `resource-server`, di file `src/main/resources/application.properties`

        spring.oauth2.resource.jwt.keyValue=-----BEGIN PUBLIC KEY----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlOJzUtkJF0L8dLm1Ia8HO65CJq+YGRXh5ygGgmY2UMeEbRyTlyuGvxPwC5VieTF2A12CZuZHIZi9YNGA0p5Gw7PExLkYTrjVymr5vhzNmX5o/98Gd7bBSgidNs4bPFoi1Dh7AdlTTgSobZRGvB9X5wyxYPNfE7BqQAlWwm/8fHDB61HMbACXbphSAspSYgfAcFrAj4wcO2HQH/Uil7ObC3O5co+Rcpc9qHuQ0KxOgh1WyZhGW7t3Oo+zYx4SAydxrlfZ/eue0nnubDoxkQFQjspF9qGhqI7k+9tGZrqdKl3ckaBQ9YtN5MkhF4S6dy5ywS3xMCBA3r72GK0/U/seMQIDAQAB\n-----END PUBLIC KEY-----

    atau
        spring.oauth2.resource.jwt.keyUri=http://localhost:8080/oauth/token_key
        

5. Contoh Token JWT

    * Access Token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiIHdyaXRlIiwicmVhZCJdLCJleHAiOjE1Mzc1NzU2MzcsImF1dGhvcml0aWVzIjpbIkNPTkZJR1VSRV9TWVNURU0iLCJWSUVXX0lORk8iXSwianRpIjoiMWFiZDYwNTEtYzQwNy00OWI1LThiNzctMTVkOGYwMGRmYjcxIiwiY2xpZW50X2lkIjoiY2xpZW50MDAxIn0.OLpgFFIPRQumik1Cp4F3WO5dzfWZ_nLUpV3ipxzdTTEO77a65lNX4-PtALkLPRMBg6Z56nEze5K0uMKDbutDk-PxL7rTEmKS1jZBCsMojdZyilBJ-ag2AWJMgIQERg3WF8o2iM2DZcp92VFMF9sAA23siz9nf3L5P8iJQ9m0DkVxW747IMYxMCrPb2BQTC2lm_ye1NTUD-aXIm_YQn3XCPD3Yg4j_jHU4mQV1FQdopx0ZfN9Md2VeeQE6nSFtJ3BxIvTRPdkTqdm5M3ahaBfr5BAR364a7bi5mdxEcIHiJ72wOS1P_Z73WJddTrPvoyt7g910rdHyiycTyWo1z8efw

    * Refresh Token : eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsiIHdyaXRlIiwicmVhZCJdLCJhdGkiOiIxYWJkNjA1MS1jNDA3LTQ5YjUtOGI3Ny0xNWQ4ZjAwZGZiNzEiLCJleHAiOjE1NDAxMjQ0MzcsImF1dGhvcml0aWVzIjpbIkNPTkZJR1VSRV9TWVNURU0iLCJWSUVXX0lORk8iXSwianRpIjoiNmQwMjhlN2UtN2Y3Ni00MDE0LTkwN2EtYTdiYWNmMzc5OWRiIiwiY2xpZW50X2lkIjoiY2xpZW50MDAxIn0.Cp93igq66P508EDLkKpGAjUG64dzweSALkkguSiATPEsZPmLxB1DpJ1VnbG2g2RTCczS-YhtbkwKzcMQVT98LwGf5gJfYHvBVeeMPYm82LA5wBNWGn702VQC9m9mLD_lkx_ldvx4MuFmpmALt8w4yhEE_hcCnF_yXacaJ1gyPiO-x7OL4KvPTgSO54YtgolRPO9QV6IbfEcB7sz_9jTLvi5CsQPdMpQNpx6gwg_b4VlpAK9yx-cPWZ2RAxs9J9JIcVVErxhidkHDrLv8Yc-175zyPoFI5qx9XKL_aXj_mdXZIySfi2iJBzVwANPmO8sZfPP_xOsFuW_rWe6aQBwFgg

6. JWT Token bisa dilihat isinya di [jwt.io](https://jwt.io)

## Flow Grant Type Implicit ##

1. Buka URL provider Authorization

        http://localhost:8080/oauth/authorize?client_id=client002&response_type=token&redirect_uri=http://localhost:10000/handle-oauth-callback

2. Login bila perlu

3. Approve di consent page

4. Mendapatkan access token di URL redirect : `http://localhost:10000/handle-oauth-callback#access_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmVsYWphciJdLCJ1c2VyX25hbWUiOiJ1c2VyMDAxIiwic2NvcGUiOlsicmVhZCIsImFkbWluIiwid3JpdGUiXSwiZXhwIjoxNTM4MTcxNTk5LCJhdXRob3JpdGllcyI6WyJDT05GSUdVUkVfU1lTVEVNIiwiVklFV19JTkZPIl0sImp0aSI6IjVjODBkNzUzLWQyZDgtNDgzZi1iZWQ3LWMyOTRkN2Q4MjliOCIsImNsaWVudF9pZCI6ImNsaWVudDAwMiJ9.HP40_3rltM3nSPMETcWGGVsJPC93z_JFoj5G6gJco8JwUIZtjSMFLHhSIlJBV1sqd0AUn_FLyPSg9PY9MQlWWG8hY3k0rChlZYldawKJaKPhjFhiuYluDbnhHSt3Hazlsx8nUzvBTsw36AFYYNU1ap1VSSl6ZwR-xDfGc3vDb_1uXL3SguUQFCzIK8GTkdEB9ecNZesLUgQa1Qce9IxZiNOL49zit8xnHgkRaYS96dzqTUphZsHdjWm0u7USSiCyWw7Yoa0-mRe8EFLEBWu2-1CFiK4qbWIS4efuxWn8gg3Y5DpaM-nhucFVUVlo2s1XZHQ0CgDEMh0MkilOY9yxLw&token_type=bearer&expires_in=43199&scope=read%20admin%20write&jti=5c80d753-d2d8-483f-bed7-c294d7d829b8`
