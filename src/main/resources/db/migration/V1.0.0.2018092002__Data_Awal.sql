INSERT INTO s_permission (id, permission_value, permission_label) VALUES
  ('configuresystem', 'CONFIGURE_SYSTEM', 'Configure System');

INSERT INTO s_permission (id, permission_value, permission_label) VALUES
  ('viewinfo', 'VIEW_INFO', 'View Info');

INSERT INTO s_permission (id, permission_value, permission_label) VALUES
  ('infowaktu', 'INFO_WAKTU', 'Info Waktu');

INSERT INTO s_role (id, description, name) VALUES
  ('superuser', 'SUPERUSER', 'Super User'),
  ('clientapp', 'CLIENTAPP', 'Client Application'),
  ('staff', 'STAFF', 'Staff'),
  ('manager', 'MANAGER', 'Manager');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('superuser', 'configuresystem');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('superuser', 'viewinfo');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('staff', 'infowaktu');

INSERT INTO s_user (id, active, username, id_role) VALUES
  ('u001', true, 'user001', 'superuser');

INSERT INTO s_user (id, active, username, id_role) VALUES
  ('u002', true, 'user002', 'staff');

INSERT INTO s_user_password (id_user, password) VALUES
  -- password : abcd
  ('u001', '$2a$10$SQdrjU5ePvnihz4BlgTf6.Tg5F1TbrWT3BDGrVuTDtT/q7HR92u0S');

INSERT INTO s_user_password (id_user, password) VALUES
  -- password : abcd
  ('u002', '$2a$10$SQdrjU5ePvnihz4BlgTf6.Tg5F1TbrWT3BDGrVuTDtT/q7HR92u0S');