package com.artivisi.training.microservice.auth;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthApplicationTests {

	@Autowired private PasswordEncoder passwordEncoder;

	@Test
	public void generatePassword() {
		String plainPassword = "abcd";
		String hashedPassword = passwordEncoder.encode(plainPassword);
		Assert.assertNotNull(hashedPassword);
		System.out.println(plainPassword+":"+hashedPassword);
	}

	
}
